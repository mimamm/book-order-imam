package com.example.BookOrderImam.views;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vm_book", schema = "public")
public class VmBook {
	private int nomor;
	private String judulBuku;
	private String authorFirstName;
	private String authorLastName;
	
	public VmBook() {
		// TODO Auto-generated constructor stub
	}

	public VmBook(int nomor, String judulBuku, String authorFirstName, String authorLastName) {
		super();
		this.nomor = nomor;
		this.judulBuku = judulBuku;
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
	}

	@Id
	@Column(name = "nomor", unique = true, nullable = false)
	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	@Column(name = "judul_buku")
	public String getJudulBuku() {
		return judulBuku;
	}

	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}

	@Column(name = "author_first_name")
	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	@Column(name = "author_last_name")
	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
	
}