package com.example.BookOrderImam.views;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vm_paper", schema = "public")
public class VmPaper {
	private int nomor;
	private String qualityName;
	private BigDecimal paperPrice;
	
	public VmPaper() {
		// TODO Auto-generated constructor stub
	}

	public VmPaper(int nomor, String qualityName, BigDecimal paperPrice) {
		super();
		this.nomor = nomor;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}

	@Id
	@Column(name = "nomor", unique = true, nullable = false)
	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	@Column(name = "quality_name")
	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	@Column(name = "paper_price")
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	
}