package com.example.BookOrderImam.views;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vm_order", schema = "public")
public class VmOrder {
	private int nomor;
	private String customerName;
	private String title;
	private int quantity;
	private BigDecimal totalOrder;
	private Date orderDate;
	
	public VmOrder() {
		// TODO Auto-generated constructor stub
	}

	public VmOrder(int nomor, String customerName, String title, int quantity, BigDecimal totalOrder, Date orderDate) {
		super();
		this.nomor = nomor;
		this.customerName = customerName;
		this.title = title;
		this.quantity = quantity;
		this.totalOrder = totalOrder;
		this.orderDate = orderDate;
	}

	@Id
	@Column(name = "nomor", unique = true, nullable = false)
	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	@Column(name = "customer_name")
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "quantity")
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Column(name = "total_order")
	public BigDecimal getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}

	@Column(name = "order_date")
	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
}
