package com.example.BookOrderImam.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @NoArgsConstructor @AllArgsConstructor
public class PublisherDto {
	private Long publisherId;
	private String companyName;
	private String country;
	private Long paperId;
	private String qualityName;
}