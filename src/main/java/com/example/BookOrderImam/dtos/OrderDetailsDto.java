package com.example.BookOrderImam.dtos;

import java.math.BigDecimal;

import com.example.BookOrderImam.models.OrderDetailsKey;

public class OrderDetailsDto {
	private OrderDetailsKey id;
	private int quantity;
	private BigDecimal discount;
	private BigDecimal tax;
	private BookDto book;
	private OrderDto order;

	public OrderDetailsDto() {
		// TODO Auto-generated constructor stub
	}

	public OrderDetailsDto(OrderDetailsKey id, int quantity, BigDecimal discount, BigDecimal tax, BookDto book,
			OrderDto order) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.book = book;
		this.order = order;
	}

	public OrderDetailsKey getId() {
		return id;
	}

	public void setId(OrderDetailsKey id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public BookDto getBook() {
		return book;
	}

	public void setBook(BookDto book) {
		this.book = book;
	}

	public OrderDto getOrder() {
		return order;
	}

	public void setOrder(OrderDto order) {
		this.order = order;
	}
	
}