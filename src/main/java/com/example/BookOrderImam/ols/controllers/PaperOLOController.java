package com.example.BookOrderImam.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.models.Paper;
import com.example.BookOrderImam.ols.PaperOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/apiolo/paper")
public class PaperOLOController extends HibernateOptionListController<Paper, PaperOLO> {

}
