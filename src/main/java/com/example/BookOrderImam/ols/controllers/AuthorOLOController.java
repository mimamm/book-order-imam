package com.example.BookOrderImam.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.models.Author;
import com.example.BookOrderImam.ols.AuthorOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/apiolo/author")
public class AuthorOLOController extends HibernateOptionListController<Author, AuthorOLO> {

}
