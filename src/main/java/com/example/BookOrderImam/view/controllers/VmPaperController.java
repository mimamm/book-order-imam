package com.example.BookOrderImam.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.view.dtos.VmPaperDto;
import com.example.BookOrderImam.views.VmPaper;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewpaper")
public class VmPaperController extends HibernateViewController<VmPaper, VmPaperDto> {

}
