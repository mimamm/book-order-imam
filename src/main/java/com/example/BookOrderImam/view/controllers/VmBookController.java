package com.example.BookOrderImam.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.view.dtos.VmBookDto;
import com.example.BookOrderImam.views.VmBook;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewbook")
public class VmBookController extends HibernateViewController<VmBook, VmBookDto> {

}
