package com.example.BookOrderImam.view.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.view.dtos.VmOrderDto;
import com.example.BookOrderImam.views.VmOrder;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/vieworder")
public class VmOrderController extends HibernateViewController<VmOrder, VmOrderDto> {

}
