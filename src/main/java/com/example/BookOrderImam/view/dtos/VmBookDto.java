package com.example.BookOrderImam.view.dtos;

public class VmBookDto {
	private int nomor;
	private String judulBuku;
	private String authorFirstName;
	private String authorLastName;
	public VmBookDto(int nomor, String judulBuku, String authorFirstName, String authorLastName) {
		super();
		this.nomor = nomor;
		this.judulBuku = judulBuku;
		this.authorFirstName = authorFirstName;
		this.authorLastName = authorLastName;
	}
	
	public VmBookDto() {
		// TODO Auto-generated constructor stub
	}

	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	public String getJudulBuku() {
		return judulBuku;
	}

	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}

	public String getAuthorFirstName() {
		return authorFirstName;
	}

	public void setAuthorFirstName(String authorFirstName) {
		this.authorFirstName = authorFirstName;
	}

	public String getAuthorLastName() {
		return authorLastName;
	}

	public void setAuthorLastName(String authorLastName) {
		this.authorLastName = authorLastName;
	}
	
	
}