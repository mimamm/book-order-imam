package com.example.BookOrderImam.view.dtos;

import java.math.BigDecimal;

public class VmPaperDto {
	private int nomor;
	private String qualityName;
	private BigDecimal paperPrice;
	
	public VmPaperDto() {
		// TODO Auto-generated constructor stub
	}

	public VmPaperDto(int nomor, String qualityName, BigDecimal paperPrice) {
		super();
		this.nomor = nomor;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}

	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	
}
