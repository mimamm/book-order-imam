package com.example.BookOrderImam.view.dtos;

import java.math.BigDecimal;
import java.util.Date;

public class VmOrderDto {
	private int nomor;
	private String customerName;
	private String title;
	private int quantity;
	private BigDecimal totalOrder;
	private Date orderDate;
	
	public VmOrderDto() {
		// TODO Auto-generated constructor stub
	}

	public VmOrderDto(int nomor, String customerName, String title, int quantity, BigDecimal totalOrder,
			Date orderDate) {
		super();
		this.nomor = nomor;
		this.customerName = customerName;
		this.title = title;
		this.quantity = quantity;
		this.totalOrder = totalOrder;
		this.orderDate = orderDate;
	}

	public int getNomor() {
		return nomor;
	}

	public void setNomor(int nomor) {
		this.nomor = nomor;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	
}