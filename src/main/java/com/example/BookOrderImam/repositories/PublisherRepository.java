package com.example.BookOrderImam.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Paper;
import com.example.BookOrderImam.models.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO publisher (company_name, country, paper_id) VALUES (:companyName, :country, :paper)", nativeQuery = true)
	public void createPublisher(@Param("companyName") String companyName, @Param("country") String country, @Param("paper") Paper paper);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE publisher SET company_name = :companyName, country = :country, paper_id = :paper WHERE publisher_id = :publisherId", nativeQuery = true)
	public int updatePublisher(@Param("publisherId") Long publisherId, @Param("companyName") String companyName, @Param("country") String country, @Param("paper") Paper paper);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM publisher WHERE publisher_id = :publisherId", nativeQuery = true)
	public void deletePublisher(@Param("publisherId") Long publisherId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM publisher WHERE publisher_id = :publisherId", nativeQuery = true)
	public Publisher getPublisherById(@Param("publisherId") Long publisherId);
	
//	GET ALL
	@Query(value = "SELECT * FROM publisher", nativeQuery = true)
	public List<Publisher> getAllPublisher();
	
}