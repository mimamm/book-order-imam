package com.example.BookOrderImam.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO customer (customer_name, country, address, email, phone_number, postal_code) VALUES (:customerName, :country, :address, :email, :phoneNumber, :postalCode)", nativeQuery = true)
	public void createCustomer(@Param("customerName") String customerName,
			@Param("country") String country,
			@Param("address") String address,
			@Param("email") String email,
			@Param("phoneNumber") String phoneNumber,
			@Param("postalCode") String postalCode);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE customer set customer_name = :customerName, country = :country, address = :address, email = :email, phone_number = :phoneNumber, postal_code = :postalCode WHERE customer_id = :customerId", nativeQuery = true)
	public int updateCustomer(@Param("customerId") Long customerId,
			@Param("customerName") String customerName,
			@Param("country") String country,
			@Param("address") String address,
			@Param("email") String email,
			@Param("phoneNumber") String phoneNumber,
			@Param("postalCode") String postalCode);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM customer WHERE customer_id = :customerId", nativeQuery = true)
	public void deleteCustomer(@Param("customerId") Long customerId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM customer WHERE customer_id = :customerId", nativeQuery = true)
	public Customer getCustomerById(@Param("customerId") Long customerId);
	
//	GET ALL
	@Query(value = "SELECT * FROM customer", nativeQuery = true)
	public List<Customer> getAllCustomer();
	
}