package com.example.BookOrderImam.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Book;
import com.example.BookOrderImam.models.Rating;
import com.example.BookOrderImam.models.Reviewer;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO rating (book_id, reviewer_id, rating_score) VALUES (:book, :reviewer, :ratingScore)", nativeQuery = true)
	public void createRating(@Param("book") Book book,
			@Param("reviewer") Reviewer reviewer,
			@Param("ratingScore") int ratingScore);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE rating SET book_id = :book, reviewer_id = :reviewer, rating_score = :ratingScore WHERE rating_id = :ratingId", nativeQuery = true)
	public int updateRating(@Param("ratingId") Long ratingId,
			@Param("book") Book book,
			@Param("reviewer") Reviewer reviewer,
			@Param("ratingScore") int ratingScore);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM rating WHERE rating_id = :ratingId", nativeQuery = true)
	public void deleteRating(@Param("ratingId") Long ratingId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM rating WHERE rating_id = :ratingId", nativeQuery = true)
	public Rating getRatingById(@Param("ratingId") Long ratingId);
	
//	GET ALL
	@Query(value = "SELECT * FROM rating", nativeQuery = true)
	public List<Rating> getAllRating();
}