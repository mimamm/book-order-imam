package com.example.BookOrderImam.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Customer;
import com.example.BookOrderImam.models.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO orders (customer_id, order_date, total_order) VALUES (:customer, :orderDate, :totalOrder)", nativeQuery = true)
	public void createOrder(@Param("customer") Customer customer,
			@Param("orderDate") Date orderDate,
			@Param("totalOrder") BigDecimal totalOrder);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET customer_id = :customer, order_date = :orderDate, total_order = :totalOrder WHERE order_id = :orderId", nativeQuery = true)
	public int updateOrder(@Param("orderId") Long orderId,
			@Param("customer") Customer customer,
			@Param("orderDate") Date orderDate,
			@Param("totalOrder") BigDecimal totalOrder);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM orders WHERE order_id = :orderId", nativeQuery = true)
	public void deleteOrder(@Param("orderId") Long orderId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM orders WHERE order_id = :orderId", nativeQuery = true)
	public Order getOrderById(@Param("orderId") Long orderId);
	
//	GET ALL
	@Query(value = "SELECT * FROM orders", nativeQuery = true)
	public List<Order> getAllOrder();
	
}