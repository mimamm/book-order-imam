package com.example.BookOrderImam.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO author (first_name, last_name, gender, age, country, rating) VALUES (:firstName, :lastName, :gender, :age, :country, :rating)", nativeQuery = true)
	public void createAuthor(@Param("firstName") String firstName, 
			@Param("lastName") String lastName, 
			@Param("gender") String gender, 
			@Param("age") int age,
			@Param("country") String country, 
			@Param("rating") String rating);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE author SET first_name = :firstName, last_name = :lastName, gender = :gender, age = :age, country = :country, rating = :rating WHERE author_id = :authorId", nativeQuery = true)
	public int updateAuthor(@Param("authorId") Long authorId, 
			@Param("firstName") String firstName, 
			@Param("lastName") String lastName,
			@Param("gender") String gender,
			@Param("age") int age,
			@Param("country") String country,
			@Param("rating") String rating);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM author WHERE author_id = :authorId", nativeQuery = true)
	public void deleteAuthor(@Param("authorId") Long authorId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM author WHERE author_id = :authorId", nativeQuery = true)
	public Author getAuthorById(@Param("authorId") Long authorId);
	
//	GET ALL
	@Query(value = "SELECT * FROM author", nativeQuery = true)
	public List<Author> getAllAuthor();
	
}