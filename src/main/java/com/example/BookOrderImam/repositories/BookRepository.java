package com.example.BookOrderImam.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Author;
import com.example.BookOrderImam.models.Book;
import com.example.BookOrderImam.models.Publisher;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO book (title, release_date, author_id, publisher_id, price) VALUES (:title, :releaseDate, :author, :publisher, :price)", nativeQuery = true)
	public void createBook(@Param("title") String title, 
			@Param("releaseDate") Date releaseDate, 
			@Param("author") Author author, 
			@Param("publisher") Publisher publisher,
			@Param("price") BigDecimal price);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE book SET title = :title, release_date = :releaseDate, author_id = :author, publisher_id = :publisher, price = :price WHERE book_id = :bookId", nativeQuery = true)
	public void updateBook(@Param("bookId") Long bookId, 
			@Param("title") String title, 
			@Param("releaseDate") Date releaseDate, 
			@Param("author") Author author, 
			@Param("publisher") Publisher publisher,
			@Param("price") BigDecimal price);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM book WHERE book_id = :bookId", nativeQuery = true)
	public void deleteBook(@Param("bookId") Long bookId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM book WHERE book_id = :bookId", nativeQuery = true)
	public Book getBookById(@Param("bookId") Long bookId);
	
//	GET ALL
	@Query(value = "SELECT * FROM book", nativeQuery = true)
	public List<Book> getAllBook();
	
}