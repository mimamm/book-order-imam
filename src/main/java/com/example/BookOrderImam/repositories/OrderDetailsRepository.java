package com.example.BookOrderImam.repositories;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.OrderDetails;
import com.example.BookOrderImam.models.OrderDetailsKey;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, OrderDetailsKey> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO order_details (book_id, order_id, discount, quantity, tax) VALUES (:bookId, :orderId, :discount, :quantity, :tax)", nativeQuery = true)
	public void createOrderDetails(@Param("bookId") Long bookId, 
			@Param("orderId") Long orderId, 
			@Param("discount") BigDecimal discount, 
			@Param("quantity") int quantity, 
			@Param("tax") BigDecimal tax);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE order_details SET discount = :discount, quantity = :quantity, tax = :tax WHERE book_id = :bookId AND order_id = :orderId", nativeQuery = true)
	public void updateOrderDetails(@Param("bookId") Long bookId, 
			@Param("orderId") Long orderId, 
			@Param("discount") BigDecimal discount, 
			@Param("quantity") int quantity, 
			@Param("tax") BigDecimal tax);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM order_details WHERE book_id = :bookId AND order_id = :orderId", nativeQuery = true)
	public void deleteOrderDetails(@Param("bookId") Long bookId, @Param("orderId") Long orderId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM order_details WHERE book_id = :bookId AND order_id = :orderId", nativeQuery = true)
	public OrderDetails getOrderDetailsById(@Param("bookId") Long bookId, @Param("orderId") Long orderId);
	
//	GET ALL
	@Query(value = "SELECT * FROM order_details", nativeQuery = true)
	public List<OrderDetails> getAllOrderDetails();
}