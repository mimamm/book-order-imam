package com.example.BookOrderImam.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookOrderImam.models.Reviewer;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
	
//	CREATE
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO reviewer (reviewer_name, country, verified) VALUES (:reviewerName, :country, :verified)", nativeQuery = true)
	public void createReviewer(@Param("reviewerName") String reviewerName,
			@Param("country") String country,
			@Param("verified") Boolean verified);
	
//	UPDATE
	@Transactional
	@Modifying
	@Query(value = "UPDATE reviewer SET reviewer_name = :reviewerName, country = :country, verified = :verified WHERE reviewer_id = :reviewerId", nativeQuery = true)
	public int updateReviewer(@Param("reviewerId") Long reviewerId,
			@Param("reviewerName") String reviewerName,
			@Param("country") String country,
			@Param("verified") Boolean verified);
	
//	DELETE
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM reviewer WHERE reviewer_id = :reviewerId", nativeQuery = true)
	public void deleteReviewer(@Param("reviewerId") Long reviewerId);
	
//	GET BY ID
	@Query(value = "SELECT * FROM reviewer WHERE reviewer_id = :reviewerId", nativeQuery = true)
	public Reviewer getReviewerById(@Param("reviewerId") Long reviewerId);
	
//	GET ALL
	@Query(value = "SELECT * FROM reviewer", nativeQuery = true)
	public List<Reviewer> getAllReviewer();
}