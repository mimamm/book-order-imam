package com.example.BookOrderImam.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.PublisherDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Publisher;
import com.example.BookOrderImam.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {  
    
    @Autowired
    PublisherRepository publisherRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a Publisher
    @PostMapping("/create")
    public Map<String, Object> createPublisher(@RequestBody PublisherDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisherEntity = modelMapper.map(body, Publisher.class);
    	
    	publisherRepository.save(publisherEntity);
    	
    	body.setPublisherId(publisherEntity.getPublisherId());
    	result.put("Message", "Create Publisher success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Publisher
    @PutMapping("/update")
    public Map<String, Object> updatePublisher(@RequestParam("publisherId") Long id, @RequestBody PublisherDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisherEntity = publisherRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Publisher", "ID", id));
    	
    	publisherEntity = modelMapper.map(body, Publisher.class);
    	publisherEntity.setPublisherId(id);
    	    	
    	publisherRepository.save(publisherEntity);
    	
    	body.setPublisherId(publisherEntity.getPublisherId());
    	result.put("Message", "Update Publisher success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Publisher
    @DeleteMapping("/delete")
    public Map<String, Object> deletePublisher(@RequestParam("publisherId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisherEntity = publisherRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Publisher", "ID", id));
    	
    	PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
    	
    	publisherRepository.delete(publisherEntity);
    	
    	result.put("Message", "Delete Publisher success");
    	result.put("Deleted Data", publisherDto);
    	
    	return result;
    }

//  Get a single Publisher by ID
    @GetMapping("/get")
    public Map<String, Object> getPublisherById(@RequestParam("publisherId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisherEntity = publisherRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Publisher", "ID", id));
    	
    	PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
    	
    	result.put("Result", publisherDto);
    	
    	return result;
    }
    
//  Get all Publisher
    @GetMapping("/getAll")
    public Map<String, Object> getAllPublisher() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Publisher> listAll = publisherRepository.findAll();
    	List<PublisherDto> listDto = new ArrayList<PublisherDto>();
    	
    	for (Publisher publisherEntity : listAll) {
			PublisherDto publisherDto = modelMapper.map(publisherEntity, PublisherDto.class);
			
			listDto.add(publisherDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}