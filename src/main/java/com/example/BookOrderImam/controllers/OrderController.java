package com.example.BookOrderImam.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.OrderDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.interfaces.DiscountRate;
import com.example.BookOrderImam.models.Order;
import com.example.BookOrderImam.models.OrderDetails;
import com.example.BookOrderImam.repositories.OrderDetailsRepository;
import com.example.BookOrderImam.repositories.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController implements DiscountRate {  
    
    @Autowired
    OrderRepository orderRepository;
    
    @Autowired
    OrderDetailsRepository orderDetailsRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Order
    @PostMapping("/create")
    public Map<String, Object> createOrder(@RequestBody OrderDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Order OrderEntity = modelMapper.map(body, Order.class);
    	
    	orderRepository.save(OrderEntity);
    	
    	body.setOrderId(OrderEntity.getOrderId());
    	result.put("Message", "Create Order success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Calculate All Total Order
    @PostMapping("/calculateTotalOrder")
    public Map<String, Object> calculateTotalOrder() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Order> orders = orderRepository.findAll();
    	
    	for (Order entity : orders) {
    		entity.setTotalOrder(calculateTotalOrder(entity));
    		
    		orderRepository.save(entity);
		}
    	
    	result.put("Message", "Calculate all Total Order success");
    	
    	return result;
    }
    
    public BigDecimal calculateTotalOrder(Order order) {
    	BigDecimal total = new BigDecimal(0);
    	
    	Set<OrderDetails> orderDetails = order.getOrderDetails();
    	
    	for (OrderDetails entity : orderDetails) {
			total = total.add(entity.getBook().getPrice().multiply(new BigDecimal(1).subtract(entity.getDiscount())).add(entity.getTax()));
		}
    	
    	return total;
    }
    
//  Calculate All Discount
    @PostMapping("/calculateAllDiscount")
    public Map<String, Object> calculateAllDiscount() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Order> orderList = orderRepository.findAll();
    	
    	for (Order entity : orderList) {
    		validateDiscount(entity);
		}
    	
    	result.put("Message", "Calculate all Discount success");
    	
    	return result;
    }
    
    public void validateDiscount(Order order) {
    	Set<OrderDetails> orderDetails = order.getOrderDetails();
    	
    	if (orderDetails.size() >= 3) {
			calculateDiscount(order);
		}
    }
    
    public void calculateDiscount(Order order) {
    	Set<OrderDetails> orderDetails = order.getOrderDetails();
    	
		for (OrderDetails entity: orderDetails) {
			entity.setDiscount(discountRate);
			
			orderDetailsRepository.save(entity);
		}
    }

//  Update a Order
    @PutMapping("/update")
    public Map<String, Object> updateOrder(@RequestParam("orderId") Long id, @RequestBody OrderDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Order OrderEntity = orderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Order", "ID", id));
    	
    	OrderEntity = modelMapper.map(body, Order.class);
    	OrderEntity.setOrderId(id);
    	
    	orderRepository.save(OrderEntity);
    	
    	body.setOrderId(OrderEntity.getOrderId());
    	result.put("Message", "Update Order success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Order
    @DeleteMapping("/delete")
    public Map<String, Object> deleteOrder(@RequestParam("orderId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Order OrderEntity = orderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Order", "ID", id));
    	
    	OrderDto OrderDto = modelMapper.map(OrderEntity, OrderDto.class);
    	
    	orderRepository.delete(OrderEntity);
    	
    	result.put("Message", "Delete Order success");
    	result.put("Deleted Data", OrderDto);
    	
    	return result;
    }

//  Get a single Order by ID
    @GetMapping("/get")
    public Map<String, Object> getOrderById(@RequestParam("orderId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Order OrderEntity = orderRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Order", "ID", id));
    	
    	OrderDto OrderDto = modelMapper.map(OrderEntity, OrderDto.class);
    	
    	result.put("Result", OrderDto);
    	
    	return result;
    }
    
 // Get all Order
    @GetMapping("/getAll")
    public Map<String, Object> getAllOrder() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Order> listAll = orderRepository.findAll();
    	List<OrderDto> listDto = new ArrayList<OrderDto>();

    	for (Order OrderEntity : listAll) {
			OrderDto OrderDto = modelMapper.map(OrderEntity, OrderDto.class);
			
			listDto.add(OrderDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}