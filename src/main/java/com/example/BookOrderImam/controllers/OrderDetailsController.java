package com.example.BookOrderImam.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.OrderDetailsDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.interfaces.TaxRate;
import com.example.BookOrderImam.models.OrderDetails;
import com.example.BookOrderImam.models.OrderDetailsKey;
import com.example.BookOrderImam.repositories.OrderDetailsRepository;

@RestController
@RequestMapping("/api/orderDetails")
public class OrderDetailsController implements TaxRate {  
    
    @Autowired
    OrderDetailsRepository orderDetailsRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Order Details
    @PostMapping("/create")
    public Map<String, Object> createOrderDetails(@RequestBody OrderDetailsDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	OrderDetails orderDetailsEntity = modelMapper.map(body, OrderDetails.class);
    	
    	orderDetailsRepository.save(orderDetailsEntity);
    	
    	body.setId(body.getId());
    	result.put("Message", "Create OrderDetails success");
    	result.put("Result", body);
    	
    	return result;
    }
        
    public BigDecimal calculateTax(OrderDetails entity) {
    	BigDecimal result = new BigDecimal(0);
    	
    	result = entity.getBook().getPrice().multiply(new BigDecimal(entity.getQuantity())).multiply(taxRate);
    	
    	return result;
    }
    
//  Calculate All Tax
    @PostMapping("/calculateTax")
    public Map<String, Object> calculateTax() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<OrderDetails> listEntity = orderDetailsRepository.findAll();
    	
    	for (OrderDetails entity : listEntity) {
			entity.setTax(calculateTax(entity));
			
			orderDetailsRepository.save(entity);
		}
    	
    	result.put("Message", "Calculate All Tax Success");
    	
    	return result;
    }

//  Update a Order Details
    @PutMapping("/update")
    public Map<String, Object> updateOrderDetails(@RequestParam("orderId") Long orderId, @RequestParam("bookId") Long bookId, @RequestBody OrderDetailsDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
		orderDetailsKey.setOrderId(orderId);
		orderDetailsKey.setBookId(bookId);
    	
    	OrderDetails orderDetailsEntity = orderDetailsRepository.findById(orderDetailsKey).orElseThrow(()-> new ResourceNotFoundException("OrderDetails", "orderDetailsId", orderDetailsKey));
    	
    	orderDetailsEntity = modelMapper.map(body, OrderDetails.class);
    	orderDetailsEntity.setOrderKey(orderDetailsKey);;
    	
    	orderDetailsRepository.save(orderDetailsEntity);
    	
    	body.setId(orderDetailsEntity.getOrderKey());
    	result.put("Message", "Update OrderDetails success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Order Details
    @DeleteMapping("/delete")
    public Map<String, Object> deleteOrderDetails(@RequestParam("orderId") Long orderId, @RequestParam("bookId") Long bookId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
		orderDetailsKey.setOrderId(orderId);
		orderDetailsKey.setBookId(bookId);
    	
    	OrderDetails orderDetailsEntity = orderDetailsRepository.findById(orderDetailsKey).orElseThrow(()-> new ResourceNotFoundException("OrderDetails", "orderDetailsId", orderDetailsKey));
    	
    	OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
    	
    	orderDetailsRepository.delete(orderDetailsEntity);
    	
    	result.put("Message", "Delete OrderDetails success");
    	result.put("Deleted Data", orderDetailsDto);
    	
    	return result;
    }

//  Get a single Order Details by ID
    @GetMapping("/get")
    public Map<String, Object> getOrderDetailsById(@RequestParam("orderId") Long orderId, @RequestParam("bookId") Long bookId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	OrderDetailsKey orderDetailsKey = new OrderDetailsKey();
		orderDetailsKey.setOrderId(orderId);
		orderDetailsKey.setBookId(bookId);
    	
    	OrderDetails orderDetailsEntity = orderDetailsRepository.findById(orderDetailsKey).orElseThrow(()-> new ResourceNotFoundException("OrderDetails", "orderDetailsId", orderDetailsKey));
    	
    	OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
    	
    	result.put("Result", orderDetailsDto);
    	
    	return result;
    }
    
// Get all Order Details
    @GetMapping("/getAll")
    public Map<String, Object> getAllOrderDetails() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<OrderDetails> listAll = orderDetailsRepository.findAll();
    	List<OrderDetailsDto> listDto = new ArrayList<OrderDetailsDto>();

    	for (OrderDetails orderDetailsEntity : listAll) {
			OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
			
			listDto.add(orderDetailsDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}