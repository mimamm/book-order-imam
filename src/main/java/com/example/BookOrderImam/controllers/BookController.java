package com.example.BookOrderImam.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.BookDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.interfaces.BookPriceRate;
import com.example.BookOrderImam.models.Book;
import com.example.BookOrderImam.models.Publisher;
import com.example.BookOrderImam.repositories.BookRepository;
import com.example.BookOrderImam.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController implements BookPriceRate {  
    
    @Autowired
    BookRepository bookRepository;
    
    @Autowired
    PublisherRepository publisherRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Book
    @PostMapping("/create")
    public Map<String, Object> createBook(@RequestBody BookDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Publisher publisherEntity = publisherRepository.findById(body.getPublisher().getPublisherId()).get();
    	
    	BigDecimal bookPrice = calculateBookPrice(publisherEntity);
    	
    	body.setPrice(bookPrice);
    	
    	Book bookEntity = modelMapper.map(body, Book.class);
    	
    	bookRepository.save(bookEntity);
    	
    	body.setBookId(bookEntity.getBookId());
    	result.put("Message", "Create Book success");
    	result.put("Result", body);
    	
    	return result;
    }

	private BigDecimal calculateBookPrice(Publisher publisherEntity) {
		BigDecimal bookPrice = publisherEntity.getPaper().getPaperPrice().multiply(bookPriceRate);
    	
    	return bookPrice;
	}

//  Update a Book
    @PutMapping("/update")
    public Map<String, Object> updateBook(@RequestParam("bookId") Long id, @RequestBody BookDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Book bookEntity = bookRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Book", "ID", id));
    	
    	bookEntity = modelMapper.map(body, Book.class);
    	bookEntity.setBookId(id);
    	
    	bookRepository.save(bookEntity);
    	
    	body.setBookId(bookEntity.getBookId());
    	result.put("Message", "Update Book success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Calculate All Book Price
    @PostMapping("/calculateBookPrice")
    public Map<String, Object> calculateAllBookPrice() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Book> listBook = bookRepository.findAll();
    	
    	for (Book bookEntity : listBook) {
			bookEntity.setPrice(calculateBookPrice(bookEntity.getPublisher()));
			
			bookRepository.save(bookEntity);
		}
    	
    	result.put("Message", "Calculate All Book Price Success");
    	
    	return result;
    }

//  Delete a Book
    @DeleteMapping("/delete")
    public Map<String, Object> deleteBook(@RequestParam("bookId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Book bookEntity = bookRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Book", "ID", id));
    	
    	BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
    	
    	bookRepository.delete(bookEntity);
    	
    	result.put("Message", "Delete Book success");
    	result.put("Deleted Data", bookDto);
    	
    	return result;
    }

//  Get a single Book by ID
    @GetMapping("/get")
    public Map<String, Object> getBookById(@RequestParam("bookId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Book bookEntity = bookRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Book", "ID", id));
    	
    	BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
    	
    	result.put("Result", bookDto);
    	
    	return result;
    }
    
// Get all Book
    @GetMapping("/readAll")
    public Map<String, Object> getAllBook() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Book> listAll = bookRepository.findAll();
    	List<BookDto> listDto = new ArrayList<BookDto>();
    	
    	for (Book bookEntity : listAll) {
			BookDto bookdto = modelMapper.map(bookEntity, BookDto.class);
			
			listDto.add(bookdto);
		}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}