package com.example.BookOrderImam.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.PaperDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Paper;
import com.example.BookOrderImam.repositories.PaperRepository;

@RestController
@RequestMapping("/api/paper")
public class PaperController {  
    
    @Autowired
    PaperRepository paperRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Paper
    @PostMapping("/create")
    public Map<String, Object> createPaper(@RequestBody PaperDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Paper paperEntity = modelMapper.map(body, Paper.class);
    	
    	paperRepository.save(paperEntity);
    	
    	body.setPaperId(paperEntity.getPaperId());
    	result.put("Message", "Create Paper success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Paper
    @PutMapping("/update")
    public Map<String, Object> updatePaper(@RequestParam("paperId") Long id, @RequestBody PaperDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Paper paperEntity = paperRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Paper", "ID", id));
    	
    	paperEntity = modelMapper.map(body, Paper.class);
    	paperEntity.setPaperId(id);
    	
    	paperRepository.save(paperEntity);
    	
    	body.setPaperId(paperEntity.getPaperId());
    	result.put("Message", "Update Paper success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Paper
    @DeleteMapping("/delete")
    public Map<String, Object> deletePaper(@RequestParam("paperId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Paper paperEntity = paperRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Paper", "ID", id));
    	
    	PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
    	
    	paperRepository.delete(paperEntity);
    	
    	result.put("Message", "Delete Paper success");
    	result.put("Deleted Data", paperDto);
    	
    	return result;
    }

//  Get a single Paper by ID
    @GetMapping("/get")
    public Map<String, Object> getPaperById(@RequestParam("paperId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Paper paperEntity = paperRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Paper", "ID", id));
    	
    	PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
    	
    	result.put("Result", paperDto);
    	
    	return result;
    }
    
// Get all Paper
    @GetMapping("/getAll")
    public Map<String, Object> getAllPaper() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Paper> listAll = paperRepository.findAll();
    	List<PaperDto> listDto = new ArrayList<PaperDto>();

    	for (Paper paperEntity : listAll) {
			PaperDto paperDto = modelMapper.map(paperEntity, PaperDto.class);
			
			listDto.add(paperDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}