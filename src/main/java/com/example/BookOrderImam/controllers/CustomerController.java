package com.example.BookOrderImam.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.CustomerDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Customer;
import com.example.BookOrderImam.repositories.CustomerRepository;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {  
    
    @Autowired
    CustomerRepository customerRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Customer
    @PostMapping("/create")
    public Map<String, Object> createCustomer(@RequestBody CustomerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Customer customerEntity = modelMapper.map(body, Customer.class);
    	
    	customerRepository.save(customerEntity);
    	
    	body.setCustomerId(customerEntity.getCustomerId());
    	result.put("Message", "Create Customer success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Customer
    @PutMapping("/update")
    public Map<String, Object> updateCustomer(@RequestParam("customerId") Long id, @RequestBody CustomerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Customer customerEntity = customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Customer", "ID", id));
    	
    	customerEntity = modelMapper.map(body, Customer.class);
    	customerEntity.setCustomerId(id);
    	
    	customerRepository.save(customerEntity);
    	
    	body.setCustomerId(customerEntity.getCustomerId());
    	result.put("Message", "Update Customer success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Customer
    @DeleteMapping("/delete")
    public Map<String, Object> deleteCustomer(@RequestParam("customerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Customer customerEntity = customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Customer", "ID", id));
    	
    	CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);
    	
    	customerRepository.delete(customerEntity);
    	
    	result.put("Message", "Delete Customer success");
    	result.put("Deleted Data", customerDto);
    	
    	return result;
    }

//  Get a single Customer by ID
    @GetMapping("/get")
    public Map<String, Object> getCustomerById(@RequestParam("customerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Customer customerEntity = customerRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Customer", "ID", id));
    	
    	CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);
    	
    	result.put("Result", customerDto);
    	
    	return result;
    }
    
//  Get all Customer
    @GetMapping("/getAll")
    public Map<String, Object> getAllCustomer() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Customer> listAll = customerRepository.findAll();
    	List<CustomerDto> listDto = new ArrayList<CustomerDto>();

    	for (Customer customerEntity : listAll) {
			CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);
			
			listDto.add(customerDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}