package com.example.BookOrderImam.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.RatingDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Rating;
import com.example.BookOrderImam.repositories.RatingRepository;

@RestController
@RequestMapping("/api/rating")
public class RatingController {  
    
    @Autowired
    RatingRepository ratingRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Rating
    @PostMapping("/create")
    public Map<String, Object> createRating(@RequestBody RatingDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = modelMapper.map(body, Rating.class);
    	
    	ratingRepository.save(ratingEntity);
    	
    	body.setRatingId(ratingEntity.getRatingId());
    	result.put("Message", "Create Rating success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Rating
    @PutMapping("/update")
    public Map<String, Object> updateRating(@RequestParam("ratingId") Long id, @RequestBody RatingDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = ratingRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Rating", "ID", id));
    	
    	ratingEntity = modelMapper.map(body, Rating.class);
    	ratingEntity.setRatingId(id);
    	
    	ratingRepository.save(ratingEntity);
    	
    	body.setRatingId(ratingEntity.getRatingId());
    	result.put("Message", "Update Rating success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Rating
    @DeleteMapping("/delete")
    public Map<String, Object> deleteRating(@RequestParam("ratingId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = ratingRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Rating", "ID", id));
    	
    	RatingDto ratingDto = modelMapper.map(ratingEntity, RatingDto.class);
    	
    	ratingRepository.delete(ratingEntity);
    	
    	result.put("Message", "Delete Rating success");
    	result.put("Deleted Data", ratingDto);
    	
    	return result;
    }

//  Get a single Rating by ID
    @GetMapping("/get")
    public Map<String, Object> getRatingById(@RequestParam("ratingId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = ratingRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Rating", "ID", id));
    	
    	RatingDto ratingDto = modelMapper.map(ratingEntity, RatingDto.class);
    	
    	result.put("Result", ratingDto);
    	
    	return result;
    }
    
 // Get all Rating
    @GetMapping("/getAll")
    public Map<String, Object> getAllRating() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Rating> listEntity = ratingRepository.findAll();
    	List<RatingDto> listDto = new ArrayList<RatingDto>();
    	
    	for (Rating ratingEntity : listEntity) {
    		RatingDto ratingDto = modelMapper.map(ratingEntity, RatingDto.class);
    		
    		listDto.add(ratingDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}