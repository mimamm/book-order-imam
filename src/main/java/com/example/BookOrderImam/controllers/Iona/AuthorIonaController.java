package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.AuthorDto;
import com.example.BookOrderImam.models.Author;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/author")
public class AuthorIonaController extends HibernateCRUDController<Author, AuthorDto> {

}