package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.OrderDetailsDto;
import com.example.BookOrderImam.models.OrderDetails;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/orderDetails")
public class OrderDetailsIonaController extends HibernateCRUDController<OrderDetails, OrderDetailsDto> {

}
