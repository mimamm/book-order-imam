package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.PaperDto;
import com.example.BookOrderImam.models.Paper;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/paper")
public class PaperIonaController extends HibernateCRUDController<Paper, PaperDto> {

}
