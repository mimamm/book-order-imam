package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.RatingDto;
import com.example.BookOrderImam.models.Rating;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/rating")
public class RatingIonaController extends HibernateCRUDController<Rating, RatingDto> {

}
