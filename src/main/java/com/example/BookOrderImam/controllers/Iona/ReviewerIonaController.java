package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.ReviewerDto;
import com.example.BookOrderImam.models.Reviewer;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/reviewer")
public class ReviewerIonaController extends HibernateCRUDController<Reviewer, ReviewerDto> {

}
