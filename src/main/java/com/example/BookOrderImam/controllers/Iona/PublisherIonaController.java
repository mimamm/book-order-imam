package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.PublisherDto;
import com.example.BookOrderImam.models.Publisher;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/publisher")
public class PublisherIonaController extends HibernateCRUDController<Publisher, PublisherDto> {

}
