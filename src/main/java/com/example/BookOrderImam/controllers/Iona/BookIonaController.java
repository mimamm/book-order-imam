package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.BookDto;
import com.example.BookOrderImam.models.Book;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/book")
public class BookIonaController extends HibernateCRUDController<Book, BookDto> {

}
