package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.OrderDto;
import com.example.BookOrderImam.models.Order;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/order")
public class OrderIonaController extends HibernateCRUDController<Order, OrderDto> {

}
