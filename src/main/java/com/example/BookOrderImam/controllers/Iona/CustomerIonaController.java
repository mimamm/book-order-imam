package com.example.BookOrderImam.controllers.Iona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.CustomerDto;
import com.example.BookOrderImam.models.Customer;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/apiIona/customer")
public class CustomerIonaController extends HibernateCRUDController<Customer, CustomerDto> {

}
