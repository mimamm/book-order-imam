package com.example.BookOrderImam.controllers.customs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.PublisherDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Paper;
import com.example.BookOrderImam.models.Publisher;
import com.example.BookOrderImam.repositories.PaperRepository;
import com.example.BookOrderImam.repositories.PublisherRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomAfterViewDetail;
import com.io.iona.springboot.actionflows.custom.CustomBeforeInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/publishercustom")
public class PublisherCustomController extends HibernateCRUDController<Publisher, PublisherDto>
implements CustomBeforeInsert<Publisher, PublisherDto>, CustomAfterReadAll<Publisher, PublisherDto>,
CustomAfterViewDetail<Publisher, PublisherDto>{
	
	@Autowired
	PublisherRepository publisherRepository;
	
	@Autowired
	PaperRepository paperRepository;

	ModelMapper modelMapper = new ModelMapper();
	public PublisherDto convertPublisherToDto (Publisher publisher) {
		PublisherDto publisherDto = modelMapper.map(publisher, PublisherDto.class);
		return publisherDto;
	}
	
	@Override
	public void beforeInsert(HibernateDataUtility dataUtility, HibernateDataSource<Publisher, PublisherDto> dataSource)
			throws Exception {
		 Publisher publisher = dataSource.getDataModel();
		 PublisherDto dto = convertPublisherToDto(publisher);
		 paperRepository.findById(dto.getPaperId()).orElseThrow(() -> new ResourceNotFoundException("Paper", "id", (dto.getPaperId())));
	}
	
//	@Override
//    public Publisher onInsert(HibernateDataUtility dataUtility, HibernateDataSource<Publisher, PublisherDto> dataSource) throws Exception {
//		Publisher publisher = dataSource.getDataModel();
//        if (paperRepository.findById(publisher.getPaper().getPaperId()).isPresent() == false) {
//            Paper paper = new Paper();
//            paper.setPaperId(publisher.getPaper().getPaperId());
//            paper.setPaperPrice(new BigDecimal(0));
//            paper.setQualityName("Quality Name Baru");
//            paperRepository.save(paper);
//        }
//        publisherRepository.save(publisher);
//        return dataSource.getDataModel();
//    }
	
	@Override
    public List<PublisherDto> afterReadAll(HibernateDataUtility dataUtility, HibernateDataSource<Publisher, PublisherDto> dataSource, DefaultPagingParameter defaultPagingParameter) throws Exception {
        List<Publisher> publishers = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);        
        List<PublisherDto> dtos = new ArrayList<>();
        
        for (Publisher publisher : publishers) {
        	PublisherDto dto = convertPublisherToDto(publisher);
        	Paper paper = paperRepository.findById(publisher.getPaper().getPaperId()).orElseThrow(() -> new ResourceNotFoundException("Paper", "id", publisher.getPaper().getPaperId()));
        	dto.setQualityName(paper.getQualityName());
        	dtos.add(dto);
        }
        return dtos;
    }
	
	@Override
	public PublisherDto afterViewDetail(HibernateDataUtility dataUtility, HibernateDataSource<Publisher, PublisherDto> dataSource)
			throws Exception {
		Publisher publisher = dataSource.getDataModel();   
        PublisherDto dto = convertPublisherToDto(publisher);
        
        Paper paper = paperRepository.findById(publisher.getPaper().getPaperId()).orElseThrow(() -> new ResourceNotFoundException("Paper", "id", publisher.getPaper().getPaperId()));
        dto.setQualityName(paper.getQualityName());

        return dto;
	}
	
}