package com.example.BookOrderImam.controllers.customs;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.BookDto;
import com.example.BookOrderImam.models.Book;
import com.io.iona.springboot.actionflows.custom.CustomAfterInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/bookcustom")
public class BookCustomController extends HibernateCRUDController<Book, BookDto> implements CustomAfterInsert<Book, BookDto> {

	@Override
	public BookDto afterInsert(HibernateDataUtility arg0, HibernateDataSource<Book, BookDto> arg1) throws Exception {
		
//		BigDecimal bookPrice = publisherEntity.getPaper().getPaperPrice().multiply(bookPriceRate);
		return null;
	}

}
