package com.example.BookOrderImam.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.AuthorDto;
import com.example.BookOrderImam.exceptions.ResourceNotFoundException;
import com.example.BookOrderImam.models.Author;
import com.example.BookOrderImam.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {  
    
    @Autowired
    AuthorRepository authorRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Author
    @PostMapping("/create")
    public Map<String, Object> createAuthor(@RequestBody AuthorDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author authorEntity = modelMapper.map(body, Author.class);
    	
    	authorRepository.save(authorEntity);
    	
    	body.setAuthorId(authorEntity.getAuthorId());
    	result.put("Message", "Create Author success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update an Author
    @PutMapping("/update")
    public Map<String, Object> updateAuthor(@RequestParam("authorId") Long id, @RequestBody AuthorDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author authorEntity = authorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Author", "ID", id));
    	
    	authorEntity = modelMapper.map(body, Author.class);
    	authorEntity.setAuthorId(id);
    	
    	authorRepository.save(authorEntity);
    	
    	body.setAuthorId(authorEntity.getAuthorId());
    	result.put("Message", "Update Author success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Author
    @DeleteMapping("/delete")
    public Map<String, Object> deleteAuthor(@RequestParam("authorId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author authorEntity = authorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Author", "ID", id));
    	
    	AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
    	
    	authorRepository.delete(authorEntity);
    	
    	result.put("Message", "Delete Author success");
    	result.put("Deleted Data", authorDto);
    	
    	return result;
    }

//  Get a single Author by ID
    @GetMapping("/get")
    public Map<String, Object> getAuthorById(@RequestParam("authorId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author authorEntity = authorRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Author", "ID", id));
    	
    	AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
    	
    	result.put("Result", authorDto);
    	
    	return result;
    }
    
//  Get all Author
    @GetMapping("/getAll")
    public Map<String, Object> getAllAuthor() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Author> listEntity = authorRepository.findAll();
    	List<AuthorDto> listDto = new ArrayList<AuthorDto>();

    	for (Author authorEntity : listEntity) {
    		AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
    		
    		listDto.add(authorDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}