package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.CustomerDto;
import com.example.BookOrderImam.models.Customer;
import com.example.BookOrderImam.repositories.CustomerRepository;

@RestController
@RequestMapping("/apiQuery/customer")
public class CustomerQueryController {
	
	@Autowired
	CustomerRepository customerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//  Create a Customer
    @PostMapping("/create")
    public Map<String, Object> createCustomer(@RequestBody CustomerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	customerRepository.createCustomer(body.getCustomerName(), body.getCountry(), body.getAddress(), body.getEmail(), body.getPhoneNumber(), body.getPostalCode());
    	
    	result.put("Message", "Create Customer success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Update a Customer
    @PutMapping("/update")
    public Map<String, Object> updateCustomer(@RequestParam("customerId") Long id, @RequestBody CustomerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	customerRepository.updateCustomer(id, body.getCustomerName(), body.getCountry(), body.getAddress(), body.getEmail(), body.getPhoneNumber(), body.getPostalCode());
    	
    	result.put("Message", "Update Customer success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Delete a Customer
    @DeleteMapping("/delete")
    public Map<String, Object> deleteCustomer(@RequestParam("customerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	customerRepository.deleteCustomer(id);
    	
    	result.put("Message", "Delete Customer success");
    	
    	return result;
    }
    
//  Get a Customer by ID
    @GetMapping("/get")
    public Map<String, Object> getCustomerById(@RequestParam("customerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Customer customerEntity = customerRepository.getCustomerById(id);
    	
    	CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);
    	
    	result.put("Result", customerDto);
    	
    	return result;
    }
    
//  Get all Customer
    @GetMapping("/getAll")
    public Map<String, Object> getAllCustomer() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Customer> listAll = customerRepository.getAllCustomer();
    	List<CustomerDto> listDto = new ArrayList<CustomerDto>();

    	for (Customer customerEntity : listAll) {
			CustomerDto customerDto = modelMapper.map(customerEntity, CustomerDto.class);
			
			listDto.add(customerDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
        
}