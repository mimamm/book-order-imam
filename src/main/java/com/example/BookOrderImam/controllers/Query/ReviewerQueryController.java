package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.ReviewerDto;
import com.example.BookOrderImam.models.Reviewer;
import com.example.BookOrderImam.repositories.ReviewerRepository;

@RestController
@RequestMapping("/apiQuery/reviewer")
public class ReviewerQueryController {
	
	@Autowired
	ReviewerRepository reviewerRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//  Create a new Reviewer
    @PostMapping("/create")
    public Map<String, Object> createReviewer(@RequestBody ReviewerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	reviewerRepository.createReviewer(body.getReviewerName(), body.getCountry(), body.isVerified());

    	result.put("Message", "Create Reviewer success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Update a Reviewer
    @PutMapping("/update")
    public Map<String, Object> updateReviewer(@RequestParam("reviewerId") Long id, @RequestBody ReviewerDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	reviewerRepository.updateReviewer(id, body.getReviewerName(), body.getCountry(), body.isVerified());

    	result.put("Message", "Update Reviewer success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Delete a Reviewer
    @DeleteMapping("/delete")
    public Map<String, Object> deleteReviewer(@RequestParam("reviewerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	reviewerRepository.deleteReviewer(id);
    	
    	result.put("Message", "Delete Reviewer success");
    	
    	return result;
    }
    
//  Get a Reviewer by ID
    @GetMapping("/get")
    public Map<String, Object> getReviewerById(@RequestParam("reviewerId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Reviewer reviewerEntity = reviewerRepository.getReviewerById(id);
    	
    	ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
    	
    	result.put("Result", reviewerDto);
    	
    	return result;
    }
    
//  Get all Reviewer
    @GetMapping("/getAll")
    public Map<String, Object> getAllReviewer() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Reviewer> listAll = reviewerRepository.getAllReviewer();
    	List<ReviewerDto> listDto = new ArrayList<ReviewerDto>();

    	for (Reviewer reviewerEntity : listAll) {
			ReviewerDto reviewerDto = modelMapper.map(reviewerEntity, ReviewerDto.class);
			
			listDto.add(reviewerDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
    
}