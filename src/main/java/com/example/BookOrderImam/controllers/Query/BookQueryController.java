package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.BookDto;
import com.example.BookOrderImam.models.Book;
import com.example.BookOrderImam.repositories.BookRepository;

@RestController
@RequestMapping("/apiQuery/book")
public class BookQueryController {
	
	@Autowired
    BookRepository bookRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Create a  Book
	@PostMapping("/create")
	public Map<String, Object> createBook(@RequestBody BookDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = modelMapper.map(body, Book.class);
		
		bookRepository.createBook(body.getTitle(), body.getReleaseDate(), bookEntity.getAuthor(),
				bookEntity.getPublisher(), body.getPrice());
		
		result.put("Message", "Create Book success");
    	result.put("Result", body);
		
		return result;
	}
	
//	Update a Book
	@PutMapping("/update")
	public Map<String, Object> updateBook(@RequestParam("bookId") Long id, @RequestBody BookDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = modelMapper.map(body, Book.class);
		
		bookRepository.updateBook(id, body.getTitle(), body.getReleaseDate(), bookEntity.getAuthor(), bookEntity.getPublisher(), body.getPrice());
		
		result.put("Message", "Update Book success");
    	result.put("Result", body);
		
		return result;
	}
	
//  Delete a Book
    @DeleteMapping("/delete")
    public Map<String, Object> deleteBook(@RequestParam("bookId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	bookRepository.deleteBook(id);
    	
    	result.put("Message", "Delete Book success");
    	
    	return result;
    }
    
//  Get Book by ID
    @GetMapping("/get")
    public Map<String, Object> getBookById(@RequestParam("bookId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Book bookEntity = bookRepository.getBookById(id);
    	
    	BookDto bookDto = modelMapper.map(bookEntity, BookDto.class);
    	
    	result.put("Result", bookDto);
    	
    	return result;
    }
    
// Get all Book
    @GetMapping("/getAll")
    public Map<String, Object> getAllBook() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Book> listAll = bookRepository.getAllBook();
    	List<BookDto> listDto = new ArrayList<BookDto>();
    	
    	for (Book bookEntity : listAll) {
			BookDto bookdto = modelMapper.map(bookEntity, BookDto.class);
			
			listDto.add(bookdto);
		}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
	
}