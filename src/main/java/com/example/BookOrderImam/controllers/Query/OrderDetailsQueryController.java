package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.OrderDetailsDto;
import com.example.BookOrderImam.models.OrderDetails;
import com.example.BookOrderImam.repositories.OrderDetailsRepository;

@RestController
@RequestMapping("/apiQuery/orderDetails")
public class OrderDetailsQueryController {
	
	@Autowired
	OrderDetailsRepository orderDetailsRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//	Create Order Details
	@PostMapping("/create")
	public Map<String, Object> createOrderDetails(@RequestBody OrderDetailsDto body) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		orderDetailsRepository.createOrderDetails(body.getId().getBookId(), body.getId().getOrderId(),
				body.getDiscount(), body.getQuantity(), body.getTax());
		
		result.put("Message", "Create Order Details success");
    	result.put("Result", body);
		
		return result;
	}
	
//  Update a Order Details
    @PutMapping("/update")
    public Map<String, Object> updateOrderDetails(@RequestParam("orderId") Long orderId, @RequestParam("bookId") Long bookId, @RequestBody OrderDetailsDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
		
		orderDetailsRepository.updateOrderDetails(bookId, orderId, body.getDiscount(), body.getQuantity(), body.getTax());
		
		result.put("Message", "Update Order Details success");
    	result.put("Result", body);
		
		return result;
    }
    
//  Delete an Order Details
    @DeleteMapping("/delete")
    public Map<String, Object> deleteOrderDetails(@RequestParam("bookId") Long bookId, @RequestParam("orderId") Long orderId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	orderDetailsRepository.deleteOrderDetails(bookId, orderId);
    	
    	result.put("Message", "Delete Order Detail success");
    	
    	return result;
    }
    
//  Get Order Details by ID
    @GetMapping("/get")
    public Map<String, Object> getOrderDetailsById(@RequestParam("bookId") Long bookId, @RequestParam("orderId") Long orderId) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	OrderDetails orderDetailsEntity = orderDetailsRepository.getOrderDetailsById(bookId, orderId);
    	
    	OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
    	
    	result.put("Result", orderDetailsDto);
    	
    	return result;
    }
    
 // Get all Order Details
    @GetMapping("/getAll")
    public Map<String, Object> getAllOrderDetails() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<OrderDetails> listAll = orderDetailsRepository.getAllOrderDetails();
    	List<OrderDetailsDto> listDto = new ArrayList<OrderDetailsDto>();

    	for (OrderDetails orderDetailsEntity : listAll) {
			OrderDetailsDto orderDetailsDto = modelMapper.map(orderDetailsEntity, OrderDetailsDto.class);
			
			listDto.add(orderDetailsDto);
		}
    	result.put("Result", listDto);
    	
    	return result;
    }
}