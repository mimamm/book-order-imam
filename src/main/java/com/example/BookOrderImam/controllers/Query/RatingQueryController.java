package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.RatingDto;
import com.example.BookOrderImam.models.Rating;
import com.example.BookOrderImam.repositories.RatingRepository;

@RestController
@RequestMapping("/apiQuery/rating")
public class RatingQueryController {  
    
    @Autowired
    RatingRepository ratingRepository;
    
    ModelMapper modelMapper = new ModelMapper();

//  Create a new Rating
    @PostMapping("/create")
    public Map<String, Object> createRating(@RequestBody RatingDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = modelMapper.map(body, Rating.class);
    	
    	ratingRepository.createRating(ratingEntity.getBook(), ratingEntity.getReviewer(), body.getRatingScore());

    	result.put("Message", "Create Rating success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Update a Rating
    @PutMapping("/update")
    public Map<String, Object> updateRating(@RequestParam("ratingId") Long id, @RequestBody RatingDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = modelMapper.map(body, Rating.class);
    	
    	ratingRepository.updateRating(id, ratingEntity.getBook(), ratingEntity.getReviewer(), body.getRatingScore());

    	result.put("Message", "Update Rating success");
    	result.put("Result", body);
    	
    	return result;
    }

//  Delete a Rating
    @DeleteMapping("/delete")
    public Map<String, Object> deleteRating(@RequestParam("ratingId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	ratingRepository.deleteRating(id);
    	
    	result.put("Message", "Delete Rating success");
    	
    	return result;
    }

//  Get a Rating by ID
    @GetMapping("/get")
    public Map<String, Object> getRatingById(@RequestParam("ratingId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Rating ratingEntity = ratingRepository.getRatingById(id);
    			
    	RatingDto ratingDto = modelMapper.map(ratingEntity, RatingDto.class);
    	
    	result.put("Result", ratingDto);
    	
    	return result;
    }
    
//  Get all Rating
    @GetMapping("/getAll")
    public Map<String, Object> getAllRating() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Rating> listEntity = ratingRepository.getAllRating();
    	List<RatingDto> listDto = new ArrayList<RatingDto>();
    	
    	for (Rating ratingEntity : listEntity) {
    		RatingDto ratingDto = modelMapper.map(ratingEntity, RatingDto.class);
    		
    		listDto.add(ratingDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}