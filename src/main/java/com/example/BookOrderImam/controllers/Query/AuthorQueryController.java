package com.example.BookOrderImam.controllers.Query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookOrderImam.dtos.AuthorDto;
import com.example.BookOrderImam.models.Author;
import com.example.BookOrderImam.repositories.AuthorRepository;

@RestController
@RequestMapping("/apiQuery/author")
public class AuthorQueryController {
	
	@Autowired
	AuthorRepository authorRepository;
	
	ModelMapper modelMapper = new ModelMapper();
	
//  Create a new Author
    @PostMapping("/create")
    public Map<String, Object> createAuthor(@RequestBody AuthorDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	authorRepository.createAuthor(body.getFirstName(), body.getLastName(), body.getGender(), body.getAge(), body.getCountry(), body.getRating());
    	
    	result.put("Message", "Create Author success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Update an Author
    @PutMapping("/update")
    public Map<String, Object> updateAuthor(@RequestParam("authorId") Long id, @RequestBody AuthorDto body) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	authorRepository.updateAuthor(id, body.getFirstName(), body.getLastName(), body.getGender(), body.getAge(), body.getCountry(), body.getRating());
    	
    	result.put("Message", "Update Author success");
    	result.put("Result", body);
    	
    	return result;
    }
    
//  Delete a Author
    @DeleteMapping("/delete")
    public Map<String, Object> deleteAuthor(@RequestParam("authorId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	authorRepository.deleteAuthor(id);
    	
    	result.put("Message", "Delete Author success");
    	
    	return result;
    }
    
//  Get an Author by ID
    @GetMapping("/get")
    public Map<String, Object> getAuthorById(@RequestParam("authorId") Long id) {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	Author authorEntity = authorRepository.getAuthorById(id);
    	
    	AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
    	
    	result.put("Result", authorDto);
    	
    	return result;
    }
    
//  Get all Author
    @GetMapping("/getAll")
    public Map<String, Object> getAllAuthor() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Author> listEntity = authorRepository.getAllAuthor();
    	List<AuthorDto> listDto = new ArrayList<AuthorDto>();

    	for (Author authorEntity : listEntity) {
    		AuthorDto authorDto = modelMapper.map(authorEntity, AuthorDto.class);
    		
    		listDto.add(authorDto);
    	}
    	
    	result.put("Result", listDto);
    	
    	return result;
    }
}