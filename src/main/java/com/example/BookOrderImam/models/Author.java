package com.example.BookOrderImam.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "author")
public class Author {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "author_author_id_seq")
	@SequenceGenerator(name = "author_author_id_seq", sequenceName = "author_author_id_seq", schema = "public", allocationSize = 1)
    private Long authorId;
	
	@Column(name = "firstName",nullable = false)
	private String firstName;
	
	@Column(name = "lastName",nullable = false)
	private String lastName;
	
	@Column(name = "gender",nullable = false)
	private String gender;
	
	@Column(name = "age",nullable = false)
	private int age;
	
	@Column(name = "country",nullable = false)
	private String country;
	
	@Column(name = "rating",nullable = false)
	private String rating;
	
	@OneToMany(mappedBy = "author")
	private Set<Book> books;
	
	public Author() {
		// TODO Auto-generated constructor stub
	}

	public Author(Long authorId, String firstName, String lastName, String gender, int age, String country,
			String rating, Set<Book> books) {
		super();
		this.authorId = authorId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.age = age;
		this.country = country;
		this.rating = rating;
		this.books = books;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

}