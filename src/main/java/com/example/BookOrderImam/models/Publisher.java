package com.example.BookOrderImam.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor @AllArgsConstructor
@Table(name = "publisher")
public class Publisher {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "publisher_publisher_id_seq")
	@SequenceGenerator(name = "publisher_publisher_id_seq", sequenceName = "publisher_publisher_id_seq", schema = "public", allocationSize = 1)
	private Long publisherId;
	
	@Column(name = "companyName", nullable = false)
	private String companyName;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@ManyToOne
	@JoinColumn(name = "paperId")
	private Paper paper;
	
	@OneToMany(mappedBy = "publisher")
	private Set<Book> books;
}