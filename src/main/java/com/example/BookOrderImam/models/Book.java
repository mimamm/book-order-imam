package com.example.BookOrderImam.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "book_book_id_seq")
	@SequenceGenerator(name = "book_book_id_seq", sequenceName = "book_book_id_seq", schema = "public", allocationSize = 1)
	private Long bookId;
	
	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "releaseDate", nullable = false)
	private Date releaseDate;
	
	@ManyToOne
	@JoinColumn(name = "authorId")
	private Author author;
	
	@ManyToOne
	@JoinColumn(name = "publisherId")
	private Publisher publisher;
	
	@Column(name = "price")
	private BigDecimal price;
	
	@OneToMany(mappedBy = "book")
	private Set<Rating> ratings;
	
	@OneToMany(mappedBy = "book")
	private Set<OrderDetails> orderDetails;
	
	public Book() {
		// TODO Auto-generated constructor stub
	}

	public Book(Long bookId, String title, Date releaseDate, Author author, Publisher publisher, BigDecimal price,
			Set<Rating> ratings, Set<OrderDetails> orderDetails) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.ratings = ratings;
		this.orderDetails = orderDetails;
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}
	
}