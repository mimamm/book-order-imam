package com.example.BookOrderImam.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

@Entity
@Table(name = "orderDetails")
public class OrderDetails {
	
	@EmbeddedId
	private OrderDetailsKey orderKey;
	
	@Column(name = "quantity", nullable = false)
	private int quantity;
	
	@Column(name = "discount")
	private BigDecimal discount;
	
	@Column(name = "tax")
	private BigDecimal tax;
	
	@ManyToOne
	@MapsId("orderId")
	@JoinColumn(name = "orderId")
	private Order order;
	
	@ManyToOne
	@MapsId("bookId")
	@JoinColumn(name = "bookId")
	private Book book;
	
	public OrderDetails() {
		// TODO Auto-generated constructor stub
	}

	public OrderDetails(OrderDetailsKey orderKey, int quantity, BigDecimal discount, BigDecimal tax, Order order,
			Book book) {
		super();
		this.orderKey = orderKey;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.order = order;
		this.book = book;
	}

	public OrderDetailsKey getOrderKey() {
		return orderKey;
	}

	public void setOrderKey(OrderDetailsKey orderKey) {
		this.orderKey = orderKey;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public BigDecimal getTax() {
		return tax;
	}

	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
}