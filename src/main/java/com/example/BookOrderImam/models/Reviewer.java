package com.example.BookOrderImam.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reviewer")
public class Reviewer {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "reviewer_reviewer_id_seq")
	@SequenceGenerator(name = "reviewer_reviewer_id_seq", sequenceName = "reviewer_reviewer_id_seq", schema = "public", allocationSize = 1)
	private Long reviewerId;
	
	@Column(name = "reviewerName", nullable = false)
	private String reviewerName;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@Column(name = "verified", nullable = false)
	private boolean verified;
	
	@OneToMany(mappedBy = "reviewer")
	private Set<Rating> ratings;
	
	public Reviewer() {
		// TODO Auto-generated constructor stub
	}

	public Reviewer(Long reviewerId, String reviewerName, String country, boolean verified, Set<Rating> ratings) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
		this.country = country;
		this.verified = verified;
		this.ratings = ratings;
	}

	public Long getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Set<Rating> getRatings() {
		return ratings;
	}

	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}
	
}