package com.example.BookOrderImam.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
public class Rating {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rating_rating_id_seq")
	@SequenceGenerator(name = "rating_rating_id_seq", sequenceName = "rating_rating_id_seq", schema = "public", allocationSize = 1)
	private Long ratingId;
	
	@ManyToOne
	@JoinColumn(name = "bookId")
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "reviewerId")
	private Reviewer reviewer;
	
	@Column(name = "ratingScore", nullable = false)
	private int ratingScore;
	
	public Rating() {
		// TODO Auto-generated constructor stub
	}

	public Rating(Long ratingId, Book book, Reviewer reviewer) {
		super();
		this.ratingId = ratingId;
		this.book = book;
		this.reviewer = reviewer;
	}

	public Long getRatingId() {
		return ratingId;
	}

	public void setRatingId(Long ratingId) {
		this.ratingId = ratingId;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}
	
}