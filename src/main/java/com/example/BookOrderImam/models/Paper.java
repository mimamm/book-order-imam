package com.example.BookOrderImam.models;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "paper")
public class Paper {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "paper_paper_id_seq")
	@SequenceGenerator(name = "paper_paper_id_seq", sequenceName = "paper_paper_id_seq", schema = "public", allocationSize = 1)
	private Long paperId;
	
	@Column(name = "qualityName", nullable = false)
	private String qualityName;
	
	@Column(name = "paperPrice", nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(mappedBy = "paper")
	private Set<Publisher> publishers;
	
	public Paper() {
		// TODO Auto-generated constructor stub
	}

	public Paper(Long paperId, String qualityName, BigDecimal paperPrice, Set<Publisher> publishers) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publishers = publishers;
	}

	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}
	
}