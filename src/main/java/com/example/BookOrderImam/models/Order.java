package com.example.BookOrderImam.models;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "orders")
public class Order {
	
//	Attributes
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "orders_order_id_seq")
	@SequenceGenerator(name = "orders_order_id_seq", sequenceName = "orders_order_id_seq", schema = "public", allocationSize = 1)
	private Long orderId;
	
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
	
	@Column(name = "orderDate", nullable = false)
	private Date orderDate;
	
	@Column(name = "totalOrder")
	private BigDecimal totalOrder;
	
	@OneToMany(mappedBy = "order")
	private Set<OrderDetails> orderDetails;
	
	public Order() {
		// TODO Auto-generated constructor stub
	}

	public Order(Long orderId, Customer customer, Date orderDate, BigDecimal totalOrder,
			Set<OrderDetails> orderDetails) {
		super();
		this.orderId = orderId;
		this.customer = customer;
		this.orderDate = orderDate;
		this.totalOrder = totalOrder;
		this.orderDetails = orderDetails;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public BigDecimal getTotalOrder() {
		return totalOrder;
	}

	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}

	public Set<OrderDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}
	
}