package com.example.BookOrderImam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookOrderImamApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookOrderImamApplication.class, args);
	}

}
